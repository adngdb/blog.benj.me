Title: Some tips to work with Github effectively
Date: 2019-10-09 19:00
Author: Benjamin Bouvier
Tags: mozilla, libre
Slug: github-pull-requests
Status: draft
Lang: en

Since I've been working on a [Github
project](https://github.com/CraneStation/Cranelift) for a while now, I've
thought now would be a good time to gather ways to make it easier to work with
Github pull requests (PRs). In particular, it's easy to drown yourself in the
incoming flow of Github emails.

This post is for you if:

- you get lost in tracking which pull requests need attention from you, be it
  either review requests or just mentions.
- you would like to strike a better work-life balance when it gets to Github
  notifications.
- you would like to filter Github email notifications in smarter ways.

Here are a few tricks I've collected over the years that make it easier to deal
with a few things, focusing on Github notifications and emails, since they were
the largest issue for me. This is not an exhaustive list of all the nice
features Github has, or all the WebExtensions that could help with Github: it
is a few things that work for me and are worth sharing. Note that I go from the
most mundane to the more specific advices here.

### Notifications dashboard

If you're working on several projects, Github can send you so many email
notifications.

It's possible to disable some kinds of notifications entirely [in the
settings](https://github.com/settings/notifications), but that's too radical
for my needs.

However, Github has a [notification
dashboard](https://github.com/notifications) that displays all the activity
related to repositories you're watching or issues/pull-requests you're involved
in. It's easy to dismiss all the notifications of all projects at once, or per
project. There's a tab on the left that allows to select more precisely your
level of involvement in the issue: did you participate in it? You can also save
some notifications for later, so they're not deleted once you've clicked them;
they'll appear under the "Saved for later" tab (I just discovered this!).

Note that Github may also send these notifications by email, if you've decided
to do so. In this case, I'd strongly recommend allowing the downloads of images
in Github emails. Despite the bad effect on your privacy this might have by
allowing user tracking, it will also synchronize the notifications' read state,
which is nice.

![Notification dashboard
count]({static}/images/github-notification-dashboard.png)

See how I am totally in control of my notifications? Truth is, I don't need
notifications in general, because I'm usually more interested in reviews I need
to receive and give.

### Pull requests dashboard

Github allows to designate a reviewer for a pull-request. At Mozilla, we
require a formal review for each change in the code-base, unless it's really
not meaningful (like, removing trailing whitespaces). Even documentation and
tests changes may require a review, depending on the rules of the code module
you're working on.

It is very common that a pull-request is received with requests for additional
changes. In this case, it is important to explicitly **re-request a review**,
otherwise this breaks all the review tracking Github proposes.

Now Github has two interesting pages for this:

- a list of all the [pull requests you have created](https://github.com/pulls)
  and that aren't closed, so you can assign reviewers and follow PR's progress
  over time.
- a list of all the [pull requests you have been assigned as a
  reviewer](https://github.com/pulls/review-requested).

### Navigating files quicker in a repository / pull request (addon)

When I know my way around a project, I'll frequently need to see the content of
a particular file or directory, that might be a few directories deep. On
Github, this means going to the files view, clicking once per directory (at
most), and finding the file I want.

The pull request view doesn't show the directory hierarchy and which files of
which directory have been touched, which is a light inconvenience too.

Good news, everyone! There is one WebExtension called
[Octotree](https://www.octotree.io/) that adds a directory view within a panel
to the left of Github's UI. By default, it's folded and doesn't take much
space; you need to hover it with the mouse to make it appear. On pull requests,
it will show files that have been modified with the diff summary for each file.
Note the website shows features from the PRO version, but there's a free
version that addresses the needs detailed above.

This is an example of the Octotree panel on our project's repository:

![Octotree example]({static}/images/github-octotree.png)

To be honest, I haven't investigated using the search bar, which could be quite
handy for this too, especially thanks to keyboard shortcuts.

### Dealing with work and non-work projects on Github

If you're using Github for personal and work related projects, you might have
been bothered by work emails coming into your personal mailbox. That has
happened to me in the past, causing some unnecessary mental load over the
weekend and unnecessarily breaking the state of relaxation.

Fortunately, Github allows you to [redirect emails from a particular Github
Organization to a specific email
address](https://github.com/settings/notifications#organization_routing). Of
course, this only works when the repository is owned by an organization and
you're part of this organization.

I'm lucky to work on such projects at the moment. It's not a silver bullet
though, because some projects are sometimes owned by personal accounts, making
this trick useless. As far as I know, there are no good solutions in this case.

### Email filters

The biggest remaining offender certainly is Github emails, in general.
Fortunately, Github has made it easy to filter them. I'll mention examples in
the Gmail email client, since that's what we're using at work, but these apply
to any other modern email client too.

#### Filter by project

Each email coming from a specific project comes with a mailing `list-id`, which
is a specific header that some email clients know how to interpret. For
instance, in Gmail, when you click on the small arrow next to the list of
recipients, you'll see many details about the current email, including, if
there's one, the "mailing-list" id, and a link to automatically create a filter
for this mailing-list. That allows you to create a particular directory/tag in
which the filter can automatically put all the emails with this id.

#### Filter by reason

In addition to filtering by project (and this is where Gmail tags /
Thunderbird's [saved
searches](https://support.mozilla.org/en-US/kb/using-saved-searches) truly
shine), it's also possible to infer more information from the Github email
notifications, by looking at the list of recipients or custom email headers.

Indeed, when there's a specific reason why an email was sent to you, Github
will add a (fake) recipient in the CC field, its address username being the
reason why the email was sent to you. For instance, in an email telling me that
somebody requested a review to me, the email address
`review_requested@github.com` will appear in the CC list. If you look at the
full message, you'll also see the custom email header `X-GitHub-Reason` set to
`review_requested`.

All the possible reasons are [detailed in Github's
documentation](https://help.github.com/en/articles/about-email-notifications#filtering-email-notifications).

These extra CC email addresses and email headers allow creating very powerful
filters that add supplementary tags to an email. For me, they relate directly
to the *importance* of the incoming email: reviews and mentions are usually
something I pay close very attention to, and get filtered in a special
top-level tag in Gmail.

Here's an example of all the information you might find about a given email in
Gmail: in particular, look at the CC list and mailing-list type ids.

![Notification dashboard count]({static}/images/github-email-example.png)

Note that Gitlab also adds some [similar custom
headers](https://docs.gitlab.com/ee/workflow/notifications.html#email-headers)
that can be filtered by some powerful email clients. I won't go into detail
about those.

### One more thing, Mozillian edition

If you're working on Mozilla's code, Gecko and/or external projects, there's
this [neat addon](https://github.com/mikeconley/myqonly/) that Mike Conley
made. It will add an icon to the Firefox button bar, indicating you the number
of requests assigned to you on Github and Phabricator, as well as the number of
pending Bugzilla requests.

It requires a minimal setup step for Github (filling your username) and
Bugzilla (adding a Bugzilla API token), and then it Just Works. It smartly
reuses a Phabricator token from the current Firefox Container's session, if
there's one.

You may think that having such a display all the time might provoke anxiety
during non-working hours. And you'd be right to think so! So the author of the
addon has added a feature to **not** display this information outside working
hours, that you can define as you like. Great stuff!

### That's it, folks!

Thanks for reading this far! I hope this helped you to some extent, allowing
you to spend less time in Github and more time doing the actual work. If you
have more interesting tips for using Github effectively, feel free to add a
comment or ping me on [twitter](https://twitter.com/bnjbvr)!
